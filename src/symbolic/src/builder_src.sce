// ====================================================================
// Allan CORNET
// Simon LIPP
// INRIA 2008
// This file is released into the public domain
// ====================================================================

src_dir = get_absolute_file_path('builder_src.sce');
tbx_builder_src_lang('cpp', src_dir);
tbx_builder_src_lang('c', src_dir);

clear tbx_builder_src_lang;
clear src_dir;

// pathT = get_absolute_file_path('buildsrc.sce');

// files = ['gw_base.o', 'scope.o', 'sym.o'];


// ilib_for_link('symbolicsrc',files,[],"c",'Makelib','loader.sce','','','-lginac -I' + pathT + '../includes');

