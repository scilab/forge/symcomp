/*
 * Symbolic module for Scilab
 * Copyright (C) 2009 - Jorge Eduardo Cardona Gaviria
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "combinatorics.hpp"


/***************************************************************
* Rising factorial
****************************************************************/

GINAC_IMPLEMENT_REGISTERED_CLASS_OPT(factorial_rising, basic,
  print_func<print_context>(&factorial_rising::do_print))

// Default constructor
factorial_rising::factorial_rising() {}

// Print the function
void factorial_rising::do_print(const print_context & c, unsigned level) const
{
    c.s << x << "^{" << n << "}";
}

int factorial_rising::compare_same_type(const basic & other) const
{
    const factorial_rising &o = static_cast<const factorial_rising &>(other);

    if (x.is_equal(o.x) and (n == o.n))
        return 0;
        
    return 1;
}

ex factorial_rising::expand(unsigned options) const
{
    // TODO: If x is numeric use a better algorithm.
    
    exvector terms;

    terms.reserve(n.to_long());
    
    terms.push_back(1);
    
    for(numeric k = 0; k < n; k++)
    {
        terms.push_back(x+k);
    }
    
    return mul(terms);
}

ex factorial_rising::derivative(const symbol & s) const
{
    return this->expand().diff(s);
}

factorial_rising::factorial_rising(numeric n, ex x) : n(n), x(x){}

/***************************************************************
* falling factorial
****************************************************************/

GINAC_IMPLEMENT_REGISTERED_CLASS_OPT(factorial_falling, basic,
  print_func<print_context>(&factorial_falling::do_print))

// Default constructor
factorial_falling::factorial_falling() {}

// Print the function
void factorial_falling::do_print(const print_context & c, unsigned level) const
{
    c.s << x << "^{" << n << "}";
}

int factorial_falling::compare_same_type(const basic & other) const
{
    const factorial_falling &o = static_cast<const factorial_falling &>(other);

    if (x.is_equal(o.x) and (n == o.n))
        return 0;
        
    return 1;
}

ex factorial_falling::expand(unsigned options) const
{
    // TODO: If x is numeric use a better algorithm.
    
    exvector terms;

    terms.reserve(n.to_long());
    
    terms.push_back(1);
    
    for(numeric k = 0; k < n; k++)
    {
        terms.push_back(x-k);
    }
    
    return mul(terms);
}

ex factorial_falling::derivative(const symbol & s) const
{
    return this->expand().diff(s);
}

factorial_falling::factorial_falling(numeric n, ex x) : n(n), x(x){}

