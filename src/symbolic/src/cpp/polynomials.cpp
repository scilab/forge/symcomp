/*
 * Symbolic module for Scilab
 * Copyright (C) 2009 - Jorge Eduardo Cardona Gaviria
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "polynomials.hpp"
#include "combinatorics.hpp"

/***************************************************************
* Chebyshev polynomial of first kind
****************************************************************/

GINAC_IMPLEMENT_REGISTERED_CLASS_OPT(chebyshev_1, basic,
  print_func<print_context>(&chebyshev_1::do_print))

// Default constructor
chebyshev_1::chebyshev_1() {}

// Print the function
void chebyshev_1::do_print(const print_context & c, unsigned level) const
{
    c.s << "Tn(" << n << "," << x << ")";
}

int chebyshev_1::compare_same_type(const basic & other) const
{
    const chebyshev_1 &o = static_cast<const chebyshev_1 &>(other);

    if (x.is_equal(o.x) and (n == o.n))
        return 0;
        
    return 1;
}

ex chebyshev_1::expand(unsigned options) const
{
    exvector terms;

    if(n.is_even())
    {
        numeric m = n/2;
        terms.reserve(m.to_long());
        
        ex seed = pow(-1,m);
        terms.push_back(seed);
        
        for(numeric k = 1; k <=m; k++)
        {
            seed *= - 2 * (m - k + 1) * (m + k - 1) * pow(x,2);
            seed /= k * (2  * k - 1);
            terms.push_back(seed);
        }
    }
    else
    {
        numeric m = (n - 1)/2;
        terms.reserve(m.to_long());

        ex seed = pow(-1,m) * (2 * m + 1) * x;
        terms.push_back(seed);
        
        for(numeric k = 1; k <= m; k++)
        {
            seed *= - 2 * (m - k + 1) * (m + k) * pow(x,2);
            seed /= k * (2 * k + 1);
            terms.push_back(seed);
        }
    }
    
    return add(terms);
}

ex chebyshev_1::derivative(const symbol & s) const
{
    return this->expand().diff(s);
/*    if(s.is_equal(x) and (n!=0))
        return n * chebyshev_2(n-1,x);
        
    return 0;*/
}

chebyshev_1::chebyshev_1(numeric n, ex x) : n(n), x(x){}


/***************************************************************
* Chebyshev polynomial of second kind
****************************************************************/

GINAC_IMPLEMENT_REGISTERED_CLASS_OPT(chebyshev_2, basic,
  print_func<print_context>(&chebyshev_2::do_print))

// Default constructor
chebyshev_2::chebyshev_2() {}

// Print the function
void chebyshev_2::do_print(const print_context & c, unsigned level) const
{
    c.s << "Un(" << n << "," << x << ")";
}

int chebyshev_2::compare_same_type(const basic & other) const
{
    const chebyshev_2 &o = static_cast<const chebyshev_2 &>(other);

    if (x.is_equal(o.x) and (n == o.n))
        return 0;
        
    return 1;
}

ex chebyshev_2::expand(unsigned options) const
{
    exvector terms;

    if(n.is_even())
    {
        numeric m = n/2;
        terms.reserve(m.to_long());
        
        ex seed = pow(-1,m);
        terms.push_back(seed);
        
        for(numeric k = 1; k <=m; k++)
        {
            seed *= - 2 * (m - k + 1) * (m + k) * pow(x,2);
            seed /= k * (2  * k - 1);
            terms.push_back(seed);
        }
    }
    else
    {
        numeric m = (n - 1)/2;
        terms.reserve(m.to_long());

        ex seed = pow(-1,m) * 2 * (m + 1) * x;
        terms.push_back(seed);
        
        for(numeric k = 1; k <= m; k++)
        {
            seed *= - 2 * (m - k + 1) * (m + k + 1) * pow(x,2);
            seed /= k * (2 * k + 1);
            terms.push_back(seed);
        }
    }
    
    return add(terms);}

ex chebyshev_2::derivative(const symbol & s) const
{
    return this->expand().diff(s);
/*    if(s.is_equal(x) and (n!=0))
        return ((n+1) * chebyshev_1(n+1,x) - x * chebyshev_2(n,x)) / (pow(x,2) - 1);

    return 0;*/
}

chebyshev_2::chebyshev_2(numeric n, symbol x) : n(n), x(x){}

/***************************************************************
* Legendre polynomial
****************************************************************/

GINAC_IMPLEMENT_REGISTERED_CLASS_OPT(legendre, basic,
  print_func<print_context>(&legendre::do_print))

// Default constructor
legendre::legendre() {}

// Print the function
void legendre::do_print(const print_context & c, unsigned level) const
{
    c.s << "Pn(" << n << "," << x << ")";
}

int legendre::compare_same_type(const basic & other) const
{
    const legendre &o = static_cast<const legendre &>(other);

    if (x.is_equal(o.x) and (n == o.n))
        return 0;
        
    return 1;
}

ex legendre::expand(unsigned options) const
{
    exvector terms;

    if(n.is_even())
    {
        numeric m = n/2;
        terms.reserve(m.to_long());
        
        ex seed = (pow(-1,m) * factorial(2 * m)) / (pow(4,m) * pow(factorial(m),2));
        terms.push_back(seed);
        
        for(numeric k = 1; k <=m; k++)
        {
            seed *= - (m - k + 1) * (2 * m + 2 * k - 1) * pow(x,2);
            seed /= k * (2  * k - 1);
            terms.push_back(seed);
        }
    }
    else
    {
        numeric m = (n - 1)/2;
        terms.reserve(m.to_long());

        ex seed = (pow(-1,m) * factorial(2 * m + 1) * x) / (pow(4,m) * pow(factorial(m),2));
        terms.push_back(seed);
        
        for(numeric k = 1; k <= m; k++)
        {
            seed *= - (m - k + 1) * (2 * m + 2 * k + 1) * pow(x,2);
            seed /= k * (2 * k + 1);
            terms.push_back(seed);
        }
    }
    
    return add(terms);
}

ex legendre::derivative(const symbol & s) const
{
    return this->expand().diff(s);
}

legendre::legendre(numeric n, symbol x) : n(n), x(x){}


/***************************************************************
* Jacobi polynomial
****************************************************************/

GINAC_IMPLEMENT_REGISTERED_CLASS_OPT(jacobi, basic,
  print_func<print_context>(&jacobi::do_print))

// Default constructor
jacobi::jacobi() {}

// Print the function
void jacobi::do_print(const print_context & c, unsigned level) const
{
    c.s << "P_" << n << "<" << a << "," << b << ">(" << x << ")";
}

int jacobi::compare_same_type(const basic & other) const
{
    const jacobi &o = static_cast<const jacobi &>(other);

    if (x.is_equal(o.x) and (n == o.n))
        return 0;
        
    return 1;
}

ex jacobi::expand(unsigned options) const
{
    exvector terms, prods;
    terms.reserve(n.to_long());


/*
    prods.reserve(n.to_long());
    
    ex seed = 1;
    for(numeric k = 1; k <=n; k++)
    {
        seed *= (n - k + 1) * (a + b + n + k) * x;
        seed /= 2 * pow(k,2);
    }

    terms.push_back(seed);
    
    
    for(numeric k = n - 1; k >=0; k--)
    {
        seed *= g;
        seed /= x * h;
    }
*/


    ex seed = 1 / factorial(n);
    
    terms.push_back(seed);
    
    for(numeric k = 1; k <=n; k++)
        seed *= (a + k);
    
    for(numeric k = 1; k <=n; k++)
    {
        seed *= (n - k + 1) * (a + b + n + k) * (x - 1);
        seed /= 2 * k * (a + k);
        terms.push_back(seed);
    }
    
    return add(terms);
}

ex jacobi::derivative(const symbol & s) const
{
    return this->expand().diff(s);
}

jacobi::jacobi(numeric n, ex a, ex b, symbol x) : n(n), a(a), b(b), x(x){}

/***************************************************************
* Laguerre polynomial
****************************************************************/

GINAC_IMPLEMENT_REGISTERED_CLASS_OPT(laguerre, basic,
  print_func<print_context>(&laguerre::do_print))

// Default constructor
laguerre::laguerre() {}

// Print the function
void laguerre::do_print(const print_context & c, unsigned level) const
{
    c.s << "L_" << n << "(" << x << ")";
}

int laguerre::compare_same_type(const basic & other) const
{
    const laguerre &o = static_cast<const laguerre &>(other);

    if (x.is_equal(o.x) and (n == o.n))
        return 0;
        
    return 1;
}

ex laguerre::expand(unsigned options) const
{
    exvector terms;
    terms.reserve(n.to_long());
    
    ex seed = 1;
    
    terms.push_back(seed);
    
    for(numeric k = 1; k <=n; k++)
    {
        seed *= -(n - k + 1) * x;
        seed /= pow(k,2);
        terms.push_back(seed);
    }
    
    return add(terms);
}

ex laguerre::derivative(const symbol & s) const
{
    return this->expand().diff(s);
}

laguerre::laguerre(numeric n, symbol x) : n(n), x(x){}

/***************************************************************
* Gegenbauer polynomial
****************************************************************/

GINAC_IMPLEMENT_REGISTERED_CLASS_OPT(gegenbauer, basic,
  print_func<print_context>(&gegenbauer::do_print))

// Default constructor
gegenbauer::gegenbauer() {}

// Print the function
void gegenbauer::do_print(const print_context & c, unsigned level) const
{
    c.s << "C^{" << a << "}_{" << n << "}(" << x << ")" ;
}

int gegenbauer::compare_same_type(const basic & other) const
{
    const gegenbauer &o = static_cast<const gegenbauer &>(other);

    if (x.is_equal(o.x) and (n == o.n))
        return 0;
        
    return 1;
}

ex gegenbauer::expand(unsigned options) const
{
    exvector terms;

    if(n.is_even())
    {
        numeric m = n/2;
        terms.reserve(m.to_long());
        
        ex seed = pow(-1,m) * factorial_rising(m,a).expand() / factorial(m);
        terms.push_back(seed);
        
        for(numeric k = 1; k <=m; k++)
        {
            seed *= - 2 * (m - k + 1) * (a + m + k - 1) * pow(x,2);
            seed /= k * (2  * k - 1);
            terms.push_back(seed);
        }
    }
    else
    {
        numeric m = (n - 1)/2;
        terms.reserve(m.to_long());
        
        ex seed = 2 * pow(-1,m) * factorial_rising(m+1,a).expand() * x/ factorial(m);
        terms.push_back(seed);
        
        for(numeric k = 1; k <=m; k++)
        {
            seed *= - 2 * (m - k + 1) * (a + m + k) * pow(x,2);
            seed /= k * (2  * k + 1);
            terms.push_back(seed);
        }
    }
    
    return add(terms);
}

ex gegenbauer::derivative(const symbol & s) const
{
    return this->expand().diff(s);
}

gegenbauer::gegenbauer(numeric n, ex a, ex x) : n(n), x(x), a(a) {}

