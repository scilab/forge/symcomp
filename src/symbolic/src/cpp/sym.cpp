/*
 * Symbolic module for Scilab
 * Copyright (C) 2009 - Jorge Eduardo Cardona Gaviria
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
extern "C"{
#include "stack-c.h"
#include "Scierror.h"
#include "api_scilab.h"
#include "api_string.h"
#include "api_common.h"
#include "api_double.h"
#include "api_boolean.h"
#include "api_int.h"
#include "api_list.h"
}
 
#include "sym.hpp"

Sym::Sym(ex e)
{
    ref = scope.new_expression(e);
}

Sym::Sym(lst l)
{
    ref = scope.new_expression(l);
}

Sym::Sym(matrix m)
{
    ref = scope.new_expression(m);
}

Sym::Sym(numeric n)
{
    ref = scope.new_expression(n);
}

Sym::Sym(function n)
{
    ref = scope.new_expression(n);
}

Sym::Sym(idx i)
{
    ref = scope.new_expression(i);
}

Sym::Sym(indexed i)
{
    ref = scope.new_expression(i);
}

Sym::Sym(symmetry s)
{
    ref = scope.new_expression(s);
}

Sym Sym::get(int k)
{
    Sym s;
    
    int* addr;
    getVarAddressFromPosition(pvApiCtx, k, &addr);
    
    int type;
    getVarType(pvApiCtx, addr, &type);
    if(type != sci_tlist)
        throw logic_error("tlist expected.");
        
    int num;
    SciErr sciErr = getListItemNumber(pvApiCtx, addr, &num);
    if(sciErr.iErr)
        throw logic_error("Error getting tlist size.");
        
    if(num != 2)
        throw logic_error("Expecting 2 items in tlist.");
    
    // Getting first item.
    int* addr1;
    sciErr = getListItemAddress(pvApiCtx, addr, 1, &addr1);
    if(sciErr.iErr)
        throw logic_error("Error getting first address.");
        
    int rows1, cols1, len1;
    sciErr = getVarDimension(pvApiCtx, addr1, &rows1, &cols1);
    if(sciErr.iErr)
        throw logic_error("Error getting datatype matrix size.");
    
    if ((rows1 != 1) || (cols1 != 1))
        throw logic_error("Expected 1x1 matrix.");

    sciErr = getMatrixOfString(pvApiCtx, addr1, &rows1, &cols1, &len1, NULL);
    if(sciErr.iErr)
        throw logic_error("Error getting datatype length.");
        
    char *buff1 = new char[len1 + 1];
    
    sciErr = getMatrixOfString(pvApiCtx, addr1, &rows1, &cols1, &len1, &buff1);
    if(sciErr.iErr)
        throw logic_error("Error getting datatype.");
    
    if (strcmp("sym",buff1))
        throw logic_error("Expecting a sym datatype.");
    
    delete [] buff1;
    
    // Getting second item.
    int* addr2;
    sciErr = getListItemAddress(pvApiCtx, addr, 2, &addr2);
    if(sciErr.iErr)
        throw logic_error("Error getting second address.");
        
    int rows2, cols2, len2;
    sciErr = getVarDimension(pvApiCtx, addr2,&rows2, &cols2);
    if(sciErr.iErr)
        throw logic_error("Error getting reference matrix size.");
    
    if ((rows2 != 1) || (cols2 != 1))
        throw logic_error("Expected 1x1 matrix.");

    sciErr = getMatrixOfString(pvApiCtx, addr2, &rows2, &cols2, &len2, NULL);
    if(sciErr.iErr)
        throw logic_error("Error getting reference length.");
        
    char *buff2 = new char[len2 + 1];

    sciErr = getMatrixOfString(pvApiCtx, addr2, &rows2, &cols2, &len2, &buff2);
    if(sciErr.iErr)
        throw logic_error("Error getting reference.");
    
    s.ref = string(buff2);
    delete [] buff2;
    
    return s;
}

void Sym::put(int k)
{
    static char * sym[] = {"sym"};
    SciErr sciErr;

    int* addr;
    sciErr = createTList(pvApiCtx, Rhs + k, 2, &addr);
    if(sciErr.iErr)
        throw logic_error("Error creating tlist.");

    sciErr = createMatrixOfStringInList(pvApiCtx, Rhs + k, addr, 1, 1, 1, sym);
    if(sciErr.iErr)
        throw logic_error("Error defining datatype.");
        
    char* buff = (char*)(ref.c_str());
    sciErr = createMatrixOfStringInList(pvApiCtx, Rhs + k, addr, 2, 1, 1, &buff);
    if(sciErr.iErr)
        throw logic_error("Error defining reference.");

    LhsVar(k) = Rhs + k;
}

bool Sym::check(int k)
{
    int* addr;
    SciErr sciErr;
    getVarAddressFromPosition(pvApiCtx, k, &addr);
    
    int type;
    getVarType(pvApiCtx, addr, &type);
    if(type != sci_tlist)
        return false;
        
    int num;
    sciErr = getListItemNumber(pvApiCtx, addr, &num);
    if(sciErr.iErr)
        return false;
        
    if(num!=2)
        return false;
    
    // Getting first item.
    int* addr1;
    sciErr = getListItemAddress(pvApiCtx, addr, 1, &addr1);
    if(sciErr.iErr)
        return false;
        
    int rows1, cols1, len1;
    sciErr = getVarDimension(pvApiCtx, addr1,&rows1, &cols1);
    if(sciErr.iErr)
        return false;
    
    if ((rows1 != 1) || (cols1 != 1))
        return false;

    sciErr = getMatrixOfString(pvApiCtx, addr1, &rows1, &cols1, &len1, NULL);
    if(sciErr.iErr)
        return false;

        
    char *buff1 = new char[len1 + 1];

    sciErr = getMatrixOfString(pvApiCtx, addr1, &rows1, &cols1, &len1, &buff1);
    if(sciErr.iErr)
        return false;
    
    if (strcmp("sym",buff1))
        return false;
    
    delete [] buff1;
    
    return true;
}

// String wrapper
String String::get(int k)
{
  SciErr sciErr;
  
  int* addr;
  getVarAddressFromPosition(pvApiCtx, k, &addr);
  
  int type;
  getVarType(pvApiCtx, addr, &type);
  if(type != sci_strings)
    throw logic_error("string expected.");
  
  int rows, cols, len;
  sciErr = getVarDimension(pvApiCtx, addr,&rows, &cols);
  if(sciErr.iErr)
    throw logic_error("Error getting matrix size.");
  
  if ((rows != 1) || (cols != 1))
    throw logic_error("Expected 1x1 matrix.");
  
  sciErr = getMatrixOfString(pvApiCtx, addr, &rows, &cols, &len, NULL);
  if(sciErr.iErr)
        throw logic_error("Error getting string.");

  char *buff = new char[len + 1];
  
  sciErr = getMatrixOfString(pvApiCtx, addr, &rows, &cols, &len, &buff);
  if(sciErr.iErr)
    throw logic_error("Error getting string.");
  
  string s(buff);
  delete [] buff;
  
  return String(s);
}

bool String::check(int k)
{
    int* addr;
    getVarAddressFromPosition(pvApiCtx, k, &addr);
    
    int type;
    getVarType(pvApiCtx, addr, &type);
    return (type == sci_strings);
}

void String::put(int k)
{
  SciErr sciErr;

  char *buff = new char[strlen(s.c_str())+1];
  strcpy(buff,s.c_str());
  
  sciErr = createMatrixOfString(pvApiCtx, Rhs + k, 1, 1, &buff);
  if(sciErr.iErr)
    throw logic_error("Error returning uint32.");
  
  LhsVar(k) = Rhs + k ;
  
}

// Double wrapper
Double Double::get(int k)
{
  SciErr sciErr;

  int* addr;
  getVarAddressFromPosition(pvApiCtx, k, &addr);
  
  int type;
  getVarType(pvApiCtx, addr, &type);
  if(type != sci_matrix)
    throw logic_error("double expected.");
  
  int rows, cols;
  sciErr = getVarDimension(pvApiCtx, addr,&rows, &cols);
  if(sciErr.iErr)
    throw logic_error("Error getting matrix size.");
    
  if ((rows != 1) || (cols != 1))
    throw logic_error("Expected 1x1 matrix.");
  
  double *data;
  sciErr = getMatrixOfDouble(pvApiCtx, addr, &rows, &cols, &data);
  if (sciErr.iErr)
    throw logic_error("Error getting double.");
    
  return Double(*data);
}

bool Double::check(int k)
{
  int* addr;
  getVarAddressFromPosition(pvApiCtx, k, &addr);
  
  int type;
  getVarType(pvApiCtx, addr, &type);
  return (type == sci_matrix);
}

void Double::put(int k)
{
  SciErr sciErr;
  
  sciErr = createMatrixOfDouble(pvApiCtx, Rhs + k, 1, 1, &d);
  if(sciErr.iErr)
    throw logic_error("Error returning uint32.");
  
  LhsVar(k) = Rhs + k ;
}
// unsigned integer wrapper
UInteger UInteger::get(int k)
{
  SciErr sciErr;
  
  int* addr;
  getVarAddressFromPosition(pvApiCtx, k, &addr);
  
  int type;
  getVarType(pvApiCtx, addr, &type);
  if(type != sci_ints)
    throw logic_error("uint32 expected.");
  
  int rows, cols;
  sciErr = getVarDimension(pvApiCtx, addr,&rows, &cols);
  if(sciErr.iErr)
    throw logic_error("Error getting matrix size.");
  
  if ((rows != 1) || (cols != 1))
    throw logic_error("Expected 1x1 matrix.");
        
  unsigned int *data;
  sciErr = getMatrixOfUnsignedInteger32(pvApiCtx, addr, &rows, &cols, &data);
  if (sciErr.iErr)
    throw logic_error("Error getting uint32.");
  
  return UInteger(*data);
}

void UInteger::put(int k)
{
  SciErr sciErr;
  
  sciErr = createMatrixOfUnsignedInteger32(pvApiCtx, Rhs + k, 1, 1, &d);
  if(sciErr.iErr)
    throw logic_error("Error returning uint32.");
  
  LhsVar(k) = Rhs + k ;
}

bool UInteger::check(int k)
{
  int* addr;
  getVarAddressFromPosition(pvApiCtx, k, &addr);
  
  int type;
  getVarType(pvApiCtx, addr, &type);
  return (type == sci_ints);
}

// bool wrapper
Bool Bool::get(int k)
{
  SciErr sciErr;
  
  int* addr;
  getVarAddressFromPosition(pvApiCtx, k, &addr);
  
  int type;
  getVarType(pvApiCtx, addr, &type);
  if(type != sci_boolean)
    throw logic_error("boolean expected.");
  
  int rows, cols;
  sciErr = getVarDimension(pvApiCtx,addr,&rows, &cols);
  if(sciErr.iErr)
    throw logic_error("Error getting matrix size.");
  
  if ((rows != 1) || (cols != 1))
    throw logic_error("Expected 1x1 matrix.");
  
  int *data;
  sciErr = getMatrixOfBoolean(pvApiCtx,addr, &rows, &cols, &data);
  if (sciErr.iErr)
    throw logic_error("Error getting boolean.");
  
  return Bool(*data);
}

void Bool::put(int k)
{
  SciErr sciErr;

  int d = (this->d)?1:0;
  sciErr = createMatrixOfBoolean(pvApiCtx, Rhs + k, 1, 1, &d);
  if(sciErr.iErr)
    throw logic_error("Error returning boolean.");
  
  LhsVar(k) = Rhs + k ;
}

bool Bool::check(int k)
{
  int* addr;
  getVarAddressFromPosition(pvApiCtx, k, &addr);
  
  int type;
  getVarType(pvApiCtx, addr, &type);
  return (type == sci_boolean);
}
