/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009 - Jorge CARDONA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __GW_FUNCTIONS__
#define __GW_FUNCTIONS__

#include "gw_base.h"

DEF_FUNCTION(sin);
DEF_FUNCTION(cos);
DEF_FUNCTION(tan);
DEF_FUNCTION(csc);
DEF_FUNCTION(sec);
DEF_FUNCTION(cot);

DEF_FUNCTION(sinh);
DEF_FUNCTION(cosh);
DEF_FUNCTION(tanh);
DEF_FUNCTION(csch);
DEF_FUNCTION(sech);
DEF_FUNCTION(coth);

DEF_FUNCTION(asin);
DEF_FUNCTION(acos);
DEF_FUNCTION(atan);
DEF_FUNCTION(acsc);
DEF_FUNCTION(asec);
DEF_FUNCTION(acot);

DEF_FUNCTION(asinh);
DEF_FUNCTION(acosh);
DEF_FUNCTION(atanh);
DEF_FUNCTION(acsch);
DEF_FUNCTION(asech);
DEF_FUNCTION(acoth);

DEF_FUNCTION(abs);
DEF_FUNCTION(step);
DEF_FUNCTION(sqrt);
DEF_FUNCTION(exp);
DEF_FUNCTION(log);

DEF_FUNCTION(gamma);
DEF_FUNCTION(log_gamma);
DEF_FUNCTION(beta);
DEF_FUNCTION(psi);

DEF_FUNCTION(new_function);
DEF_FUNCTION(call_function);

#endif /*  __GW_FUNCTIONS__ */
