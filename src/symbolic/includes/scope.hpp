/*
 * Symbolic module for Scilab
 * Copyright (C) 2009 - Jorge Eduardo Cardona Gaviria
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
#ifndef __SCOPE_HPP__
#define __SCOPE_HPP__

#include <ginac/ginac.h>
#include <sstream>

using namespace std;
using namespace GiNaC;

//TODO: Multi-level closure: it should be posible to define multilevels scope, and switch to one in particular
// with some command.

class Scope
{
public:

    // Map with symbols.
    map<string, ex> scope;
    
    Scope() : counter(0) {init_constants();};
    
    void init_constants() {
        scope["pi"] = Pi;
        scope["catalan"] = Catalan;
        scope["euler"] = Euler;
        // TODO: +inf / -inf
    };
    
    // Create a symbol.
    string new_symbol(string name);

    // Create a function
    string new_function(string name, unsigned int n);
    
    // Create an expression
    string new_expression(ex e);
    
private:
    unsigned int counter;
};


extern Scope scope;

#endif /*  __SCOPE_HPP__ */
