/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009 - Jorge CARDONA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __GW_SYMBOLIC__
#define __GW_SYMBOLIC__

#include "gw_base.h"

// Display 
DEF_FUNCTION(disp);

// Creation of basic entities
DEF_FUNCTION(symbol);
DEF_FUNCTION(number);

// Constants
DEF_FUNCTION(Pi);
DEF_FUNCTION(Catalan);
DEF_FUNCTION(Euler);

// Arithmetics
DEF_FUNCTION(add);
DEF_FUNCTION(sub);
DEF_FUNCTION(mul);
DEF_FUNCTION(res);
DEF_FUNCTION(pow);

// Arbitrary precision
DEF_FUNCTION(set_digits);
DEF_FUNCTION(get_digits);

// General
DEF_FUNCTION(evalf);
DEF_FUNCTION(expand);
DEF_FUNCTION(collect);
DEF_FUNCTION(subs);

// Scope
DEF_FUNCTION(scope_clean);
DEF_FUNCTION(scope_list);
DEF_FUNCTION(scope_get);

// Relations
DEF_FUNCTION(rel_equal);
DEF_FUNCTION(rel_not_equal);
DEF_FUNCTION(rel_less);
DEF_FUNCTION(rel_less_or_equal);
DEF_FUNCTION(rel_greater);
DEF_FUNCTION(rel_greater_or_equal);

// Checks
DEF_FUNCTION(is_numeric);
DEF_FUNCTION(is_real);
DEF_FUNCTION(is_rational);
DEF_FUNCTION(is_integer);
DEF_FUNCTION(is_crational);
DEF_FUNCTION(is_cinteger);
DEF_FUNCTION(is_positive);
DEF_FUNCTION(is_negative);
DEF_FUNCTION(is_nonnegative);
DEF_FUNCTION(is_posint);
DEF_FUNCTION(is_negint);
DEF_FUNCTION(is_nonnegint);
DEF_FUNCTION(is_even);
DEF_FUNCTION(is_odd);
DEF_FUNCTION(is_prime);
DEF_FUNCTION(is_rel);
DEF_FUNCTION(is_rel_equal);
DEF_FUNCTION(is_rel_not_equal);
DEF_FUNCTION(is_rel_less);
DEF_FUNCTION(is_rel_less_or_equal);
DEF_FUNCTION(is_rel_greater);
DEF_FUNCTION(is_rel_gerater_or_equal);

#endif /*  __GW_SYMBOLIC__ */
