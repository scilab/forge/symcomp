/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009 - Jorge CARDONA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
#ifndef __GW_BASE_H__
#define __GW_BASE_H__

#include "Scierror.h"

#define DEF_FUNCTION(x) int sci_sym_##x(char *fname); \
int sci_sym_try_catch_##x(char *fname)

#define IMP_FUNCTION(x) int sci_sym_##x(char *fname){ \
    try{ \
        return sci_sym_try_catch_##x(fname); \
    }catch(logic_error e){ \
        Scierror(999,"Logic Error: <%s>.",(char*)(e.what())); \
    }catch(range_error e){ \
        Scierror(999,"Range Error: <%s>.",(char*)(e.what())); \
    }catch(...){ \
        Scierror(999,"Unexpected Error."); \
    }} \
    \
inline int sci_sym_try_catch_##x(char *fname)


#endif /*  __GW_BASE_H__ */

