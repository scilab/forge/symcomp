/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009 - Jorge CARDONA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __POLYNOMIALS_HPP__
#define __POLYNOMIALS_HPP__

#include "ginac/ginac.h"
using namespace GiNaC;

// Chebyshev polynomial of first kind
class chebyshev_1 : public basic
{
    GINAC_DECLARE_REGISTERED_CLASS(chebyshev_1, basic)
    
public:
    // Constructor of chebyshev of first kind
    chebyshev_1(numeric n, ex x);

    ex derivative(const symbol & s) const;
    ex expand(unsigned options = 0) const;
    
protected:
    void do_print(const print_context & c, unsigned level = 0) const;
    
private:
    // TODO: add a term exvector in order to only expand once a polynomial, use nops and op.
    numeric n;
    ex x;
};

// Chebyshev polynomial of second kind
class chebyshev_2 : public basic
{
    GINAC_DECLARE_REGISTERED_CLASS(chebyshev_2, basic)
    
public:
    // Constructor of chebyshev of first kind
    chebyshev_2(numeric n, symbol x);

    ex derivative(const symbol & s) const;
    ex expand(unsigned options = 0) const;
    
protected:
    void do_print(const print_context & c, unsigned level = 0) const;
    
private:
    numeric n;
    ex x;
};

// Legendre polynomial
class legendre : public basic
{
    GINAC_DECLARE_REGISTERED_CLASS(legendre, basic)
    
public:
    // Constructor of chebyshev of first kind
    legendre(numeric n, symbol x);

    ex derivative(const symbol & s) const;
    ex expand(unsigned options = 0) const;
    
protected:
    void do_print(const print_context & c, unsigned level = 0) const;
    
private:
    numeric n;
    ex x;
};

// Legendre polynomial
class jacobi : public basic
{
    GINAC_DECLARE_REGISTERED_CLASS(jacobi, basic)
    
public:
    // Constructor of chebyshev of first kind
    jacobi(numeric n, ex a, ex b, symbol x);

    ex derivative(const symbol & s) const;
    ex expand(unsigned options = 0) const;
    
protected:
    void do_print(const print_context & c, unsigned level = 0) const;
    
private:
    numeric n;
    ex a;
    ex b;
    ex x;
};

// Laguerre polynomial
class laguerre : public basic
{
    GINAC_DECLARE_REGISTERED_CLASS(laguerre, basic)
    
public:
    // Constructor of chebyshev of first kind
    laguerre(numeric n, symbol x);

    ex derivative(const symbol & s) const;
    ex expand(unsigned options = 0) const;
    
protected:
    void do_print(const print_context & c, unsigned level = 0) const;
    
private:
    numeric n;
    ex x;
};

// Gegenbauer polynomial
class gegenbauer : public basic
{
    GINAC_DECLARE_REGISTERED_CLASS(gegenbauer, basic)
    
public:
    // Constructor of chebyshev of first kind
    gegenbauer(numeric n, ex a, ex x);

    ex derivative(const symbol & s) const;
    ex expand(unsigned options = 0) const;
    
protected:
    void do_print(const print_context & c, unsigned level = 0) const;
    
private:
    numeric n;
    ex a;
    ex x;
};

#endif /*  __POLYNOMIALS_HPP__ */
