/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009 - Jorge CARDONA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __GW_COMBINATORICS__
#define __GW_COMBINATORICS__

#include "gw_base.h"

DEF_FUNCTION(factorial);
DEF_FUNCTION(factorial_rising);
DEF_FUNCTION(factorial_falling);

#endif /*  __GW_COMBINATORICS__ */
