/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009 - Jorge CARDONA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
#ifndef __SYM_HPP__
#define __SYM_HPP__

#include <ginac/ginac.h>

extern "C" {
#include "gw_base.h"
}

#include "scope.hpp"

using namespace GiNaC;
using namespace std;

class Sym
{
public:
    Sym(){};
    Sym(string ref):ref(ref){};
    Sym(const char *ref):ref(string(ref)){};
    
    Sym(ex e);
    Sym(lst l);
    Sym(matrix m);
    Sym(numeric n);
    Sym(function n);
    Sym(idx i);
    Sym(indexed i);
    Sym(symmetry s);

    // Methods that deal with the stack
    static Sym get(int k);
    void put(int k);

    // Methods that returs GiNaC datatypes
    // TODO: Replace this with operator.
    ex to_ex(){ return scope.scope[ref];}
    lst to_lst(){ return ex_to<lst>(scope.scope[ref]);}
    matrix to_matrix(){ return ex_to<matrix>(scope.scope[ref]);}
    symbol to_symbol(){ return ex_to<symbol>(scope.scope[ref]);}
    numeric to_numeric(){ return ex_to<numeric>(scope.scope[ref]);}
    idx to_idx(){ return ex_to<idx>(scope.scope[ref]);}
    template<class T>
    T to(){ return ex_to<T>(scope.scope[ref]);}
    
    inline ex& operator() () {
        return scope.scope[ref];
    };
    
    ex  operator() () const {
        return scope.scope[ref];
    }

    operator ex()       { return scope.scope[ref]; }
    
    operator lst()          { return ex_to<lst>         (scope.scope[ref]);}
    operator matrix()       { return ex_to<matrix>      (scope.scope[ref]);}
    operator symbol()       { return ex_to<symbol>      (scope.scope[ref]);}
    operator numeric()      { return ex_to<numeric>     (scope.scope[ref]);}
    operator idx()          { return ex_to<idx>         (scope.scope[ref]);}
    operator function()     { return ex_to<function>    (scope.scope[ref]);}
    operator relational()   { return ex_to<relational>  (scope.scope[ref]);}

    static bool check(int k);
    
protected:
    // Internal reference
    string ref;

};

class String
{
//TODO: Add support to matrices of strings.
public:
    String(){};
    String(string s):s(s) {}
    
    static String get(int k);
    void put(int k);
    
    static bool check(int k);
    operator string(){ return s;}
    
protected:
    string s;
};

class Double
{
//TODO: Add support to matrices of double.
public:
    Double(){};
    Double(double d) :d(d){};

    static Double get(int k);
    void put(int k);
    
    static bool check(int k);
    operator double() {return d;}
    
protected:
    double d;
};

class UInteger
{
//TODO: Add support to matrices of integer.
public:
    UInteger(){};
    UInteger(unsigned int d):d(d){};
    
    static UInteger get(int k);
    void put(int k);
    
    static bool check(int k);
    operator unsigned int(){return d;}
    
protected:
    unsigned int d;
};

class Bool
{
public:
    Bool(){};
    Bool(bool d):d(d){};
    
    static Bool get(int k);
    void put(int k);
    
    static bool check(int k);
    operator bool(){return d;}
    
protected:
    bool d;
};

#endif /*  __SYM_HPP__ */
