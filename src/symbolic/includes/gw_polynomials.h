/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009 - Jorge CARDONA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __GW_POLYNOMIALS__
#define __GW_POLYNOMIALS__

#include "gw_base.h"

// Return the degree of a polynomial
DEF_FUNCTION(poly_degree);

// Return the low degree of a polynomial
DEF_FUNCTION(poly_ldegree);

// Return the n-th coeff of a polynomial
DEF_FUNCTION(poly_coeff);

// Return the leading coeff of a polynomial
DEF_FUNCTION(poly_lcoeff);

// Return the trailing coeff of a polynomial
DEF_FUNCTION(poly_tcoeff);

// Return the quotient of a plynomial division (a/b) and satisfy a = b*q + r
DEF_FUNCTION(poly_quo)
;
// Return the remainder of a plynomial division (a/b) and satisfy a = b*q + r
DEF_FUNCTION(poly_rem);

// Return the gcd between two polynomials
DEF_FUNCTION(poly_gcd);

// Return the lcm between two polynomials
DEF_FUNCTION(poly_lcm);

// Return the resultant between polynomials a and b.
DEF_FUNCTION(poly_resultant);

// Return the square free factorization of a polynomial
DEF_FUNCTION(poly_sqrfree);

// Return the factorization of a polynomial
DEF_FUNCTION(poly_factor);

// Return the chebyshev polynomial of first kind
DEF_FUNCTION(poly_chebyshev_T);

// Return the chebyshev polynomial of second kind
DEF_FUNCTION(poly_chebyshev_U);

// Return the legendre polynomial
DEF_FUNCTION(poly_legendre);

// Return the jacobi polynomial
DEF_FUNCTION(poly_jacobi);

// Return the laguerre polynomial
DEF_FUNCTION(poly_laguerre);

// Return the gegenbauer polynomial
DEF_FUNCTION(poly_gegenbauer);

#endif /*  __GW_POLYNOMIALS__ */
