/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009 - Jorge CARDONA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __GW_INDEXED__
#define __GW_INDEXED__

#include "gw_base.h"

// Create a index, it could be co(ntra)variant, or (un)dotted.
DEF_FUNCTION(index);
DEF_FUNCTION(indexed);

// Return the value of the index
DEF_FUNCTION(index_value);

// Return the dimension of the index
DEF_FUNCTION(index_dim);

// Return true if it has a numeric value
DEF_FUNCTION(index_is_numeric);

// Return true if it has a symbolic value
DEF_FUNCTION(index_is_symbolic);

// Return true if it has a numeric dim
DEF_FUNCTION(index_is_dim_numeric);

// Return true if it has a symbolc dim
DEF_FUNCTION(index_is_dim_symbolic);

// Return true if it is contravariant
DEF_FUNCTION(index_is_contravariant);

// Return true if it is covariant
DEF_FUNCTION(index_is_covariant);

// Return true if it is dotted
DEF_FUNCTION(index_is_dotted);

// Return true if it is undotted
DEF_FUNCTION(index_is_undotted);

// Return a new index with the inverse variance.
DEF_FUNCTION(index_toggle_variance);

// Return a new index with the inverse dottness???.
DEF_FUNCTION(index_toggle_dot);

// Return none symmetry
DEF_FUNCTION(sy_none);

// Return full symmetry
DEF_FUNCTION(sy_symm);

// Return anti symmetry
DEF_FUNCTION(sy_anti);

// Return cyclic symmetry
DEF_FUNCTION(sy_cycl);

// Return the free index on a expression
DEF_FUNCTION(get_free_indices);

// Expand sum terms with dummy indices
DEF_FUNCTION(expand_dummy_sum);

#endif /*  __GW_INDEXED__ */
