// Get current path
pathT = get_absolute_file_path('009-indexed.sce');
exec(pathT + '/../etc/symbolic.start');

// Set true and false
true = (1==1); false = (1==0);



//b = sym_index(sym_symbol("b"),sym_number("2"),false, true); disp(b);
//c = sym_index(sym_symbol("c"),sym_number("3"),true, false); disp(c);
//d = sym_index(sym_symbol("d"),sym_number("4"),true, true); disp(d);


a = sym_index(sym_symbol("a"),sym_number("1")); disp(a);
disp(sym_index_value(a));
disp(sym_index_dim(a));
disp(sym_index_is_numeric(a));
disp(sym_index_is_symbolic(a));
disp(sym_index_is_dim_numeric(a));
disp(sym_index_is_dim_symboli(a));
try
    disp(sym_index_is_contravaria(a));
    disp(sym_index_is_covariant(a));
    disp(sym_index_is_dotted(a));
    disp(sym_index_is_undotted(a));
catch
    disp("Error expected");
end;

a = sym_index(sym_symbol("a"),sym_number("1"),true); disp(a);
disp(sym_index_value(a));
disp(sym_index_dim(a));
disp(sym_index_is_numeric(a));
disp(sym_index_is_symbolic(a));
disp(sym_index_is_dim_numeric(a));
disp(sym_index_is_dim_symboli(a));
disp(sym_index_is_contravaria(a));
disp(sym_index_is_covariant(a));
try
    disp(sym_index_is_dotted(a));
    disp(sym_index_is_undotted(a));
catch
    disp("Error expected");
end;

a = sym_index(sym_symbol("a"),sym_number("1"),false); disp(a);
disp(sym_index_value(a));
disp(sym_index_dim(a));
disp(sym_index_is_numeric(a));
disp(sym_index_is_symbolic(a));
disp(sym_index_is_dim_numeric(a));
disp(sym_index_is_dim_symboli(a));
disp(sym_index_is_contravaria(a));
disp(sym_index_is_covariant(a));
try
    disp(sym_index_is_dotted(a));
    disp(sym_index_is_undotted(a));
catch
    disp("Error expected");
end;

a = sym_index(sym_symbol("a"),sym_number("1"),false,true); disp(a);
disp(sym_index_value(a));
disp(sym_index_dim(a));
disp(sym_index_is_numeric(a));
disp(sym_index_is_symbolic(a));
disp(sym_index_is_dim_numeric(a));
disp(sym_index_is_dim_symboli(a));
disp(sym_index_is_contravaria(a));
disp(sym_index_is_covariant(a));
disp(sym_index_is_dotted(a));
disp(sym_index_is_undotted(a));

