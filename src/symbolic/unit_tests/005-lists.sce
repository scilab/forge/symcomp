// Get current path
pathT = get_absolute_file_path('005-lists.sce');
exec(pathT + '/../etc/symbolic.start');

x = sym_symbol('x');
y = sym_symbol('y');
z = sym_symbol('z');

a = sym_list();
disp(a);

a = sym_list_append(a,x);
a = sym_list_append(a,y);
a = sym_list_append(a,z);
disp(a);

a = sym_list_prepend(a,x);
a = sym_list_prepend(a,y);
a = sym_list_prepend(a,z);
disp(a);

disp(sym_list_item_get(a,uint32(1)));
disp(sym_list_item_get(a,uint32(2)));
disp(sym_list_item_get(a,uint32(3)));
disp(sym_list_item_get(a,uint32(4)));
disp(sym_list_item_get(a,uint32(5)));
disp(sym_list_item_get(a,uint32(6)));

a = sym_list_item_set(a,uint32(1),y);
a = sym_list_item_set(a,uint32(2),z);
a = sym_list_item_set(a,uint32(3),z);
a = sym_list_item_set(a,uint32(4),y);
a = sym_list_item_set(a,uint32(5),x);
a = sym_list_item_set(a,uint32(6),x);
disp(a);

a = sym_list_remove_first(a);
disp(a);

a = sym_list_remove_last(a);
disp(a);

disp(sym_list_size(a));

a = sym_list_clear(a);
disp(a);

a = sym_list();
disp(a);

b = sym_list();
disp(b);


a = sym_list_append(a,x);
a = sym_list_append(a,y);
a = sym_list_append(a,z);
disp(a);

b = sym_list_prepend(b,x);
b = sym_list_prepend(b,y);
b = sym_list_prepend(b,z);
disp(b);

c = sym_list_concatenation(a,b);
disp(c);

disp('Peano will like this. :)');
deff('x=peano(s)', 'x=sym_list_append(s,s);');

b = sym_list();disp(b);
for i = 1:5
  b = peano(b); 
  disp(string(i) + ") ");  
  disp(b);
end;
