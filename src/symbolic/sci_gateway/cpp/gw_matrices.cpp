/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009 - Jorge CARDONA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
 
extern "C" {
#include "stack-c.h"
#include "gw_matrices.h"
}

#include "scope.hpp"
#include "sym.hpp"

#include "ginac/ginac.h"

using namespace GiNaC;
using namespace std;

IMP_FUNCTION(matrix_new)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,3);

    unsigned int rows, cols;
    lst l;
    
    // Get operands
    rows = UInteger::get(1);
    cols = UInteger::get(2);
    
    matrix r;
    if(Rhs == 3)
    {
        ex l = Sym::get(3);
        r = matrix(rows,cols,ex_to<lst>(l));
    }
    else
    {
        r = matrix(rows, cols);
    }

    // Return data
    Sym(r).put(1);
    return TRUE;
}

IMP_FUNCTION(matrix_diag)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    lst a = Sym::get(1);
    
    // Do something
    ex r = diag_matrix(a);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

IMP_FUNCTION(matrix_unit)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    unsigned int n = UInteger::get(1);
    
    // Do something
    ex r = unit_matrix(n);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

IMP_FUNCTION(matrix_symbolic)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(3,3);

    // Get operands
    string base = String::get(1);
    
    unsigned int rows = UInteger::get(2);
    unsigned int cols = UInteger::get(3);
        
    // Do something
    ex r = symbolic_matrix(rows, cols, base);

    // Return data
    Sym(r).put(1);

    return TRUE;
}

IMP_FUNCTION(matrix_sub)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(5,5);

    // Get operands
    matrix m            = Sym::get(1);
    unsigned int rows   = UInteger::get(2);
    unsigned int cols   = UInteger::get(3);
    unsigned int nrows  = UInteger::get(4);
    unsigned int ncols  = UInteger::get(5);

    // Do something
    ex r = sub_matrix(m, rows - 1, nrows, cols - 1, ncols);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

IMP_FUNCTION(matrix_reduced)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(3,3);

    // Get operands
    matrix m            = Sym::get(1);
    unsigned int rows   = UInteger::get(2);
    unsigned int cols   = UInteger::get(3);

    // Do something
    ex r = reduced_matrix(m, rows, cols);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

IMP_FUNCTION(matrix_item_get)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(3,3);

    // Get operands
    matrix m            = Sym::get(1);
    unsigned int row    = UInteger::get(2);
    unsigned int col    = UInteger::get(3);

    // Do something
    ex r = m(row-1, col-1);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

IMP_FUNCTION(matrix_item_set)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(4,4);

    // Get operands
    matrix m            = Sym::get(1);
    unsigned int row    = UInteger::get(2);
    unsigned int col    = UInteger::get(3);
    ex s                = Sym::get(4);

    // Do something
    m(row-1, col-1) = s;

    // Return data
    Sym(m).put(1);
    return TRUE;
}

IMP_FUNCTION(matrix_eval)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    matrix m = Sym::get(1);

    // Do something
    ex r = m.evalm();

    // Return data
    Sym(r).put(1);
    return TRUE;
}

IMP_FUNCTION(matrix_determinant)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    matrix m = Sym::get(1);

    // Do something
    ex r = m.determinant();

    // Return data
    Sym(r).put(1);
    return TRUE;
}

IMP_FUNCTION(matrix_trace)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    matrix m = Sym::get(1);

    // Do something
    ex r = m.trace();

    // Return data
    Sym(r).put(1);
    return TRUE;
}

IMP_FUNCTION(matrix_charpoly)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    matrix m = Sym::get(1);
    symbol x = Sym::get(2);

    // Do something
    ex r = m.charpoly(x);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

IMP_FUNCTION(matrix_rank)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    matrix m = Sym::get(1);

    // Do something
    unsigned int r = m.rank();

    // Return data
    UInteger(r).put(1);
    return TRUE;
}

IMP_FUNCTION(matrix_transpose)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    matrix m = Sym::get(1);

    // Do something
    matrix r = m.transpose();

    // Return data
    Sym(r).put(1);
    return TRUE;
}

IMP_FUNCTION(matrix_size)
{
    // Check input and output size
    CheckLhs(2,2); CheckRhs(1,1);

    // Get operands
    matrix m = Sym::get(1);

    // Do something
    unsigned int r = m.rows();
    unsigned int c = m.cols();

    // Return data
    UInteger(r).put(1);
    UInteger(c).put(2);
    return TRUE;
}

IMP_FUNCTION(matrix_size_cols)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    matrix m = Sym::get(1);

    // Do something
    unsigned int c = m.cols();

    // Return data
    UInteger(c).put(1);
    return TRUE;
}

IMP_FUNCTION(matrix_size_rows){
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    matrix m = Sym::get(1);

    // Do something
    unsigned int r = m.rows();

    // Return data
    UInteger(r).put(1);
    return TRUE;
}

IMP_FUNCTION(matrix_to_list)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    matrix m = Sym::get(1);

    // Do something
    unsigned int r = m.rows();
    lst l;
    for(int i=0; i < r; i++)
        l.append(m(i-1,0));

    // Return data
    Sym(l).put(1);
    return TRUE;
}

IMP_FUNCTION(matrix_solve)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(3,3);

    // Get operands
    matrix a = Sym::get(1);
    matrix b = Sym::get(2);
    matrix x = Sym::get(3);

    // Do something
    // TODO: Check options on solve
    ex r = a.solve(x,b);
    
    // Return data
    Sym(r).put(1);
    return TRUE;
}

IMP_FUNCTION(matrix_inverse)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    matrix m = Sym::get(1);

    // Do something
    matrix r = m.inverse();

    // Return data
    Sym(r).put(1);
    return TRUE;
}

