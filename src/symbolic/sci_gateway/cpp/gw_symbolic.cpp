/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009 - Jorge CARDONA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
 
extern "C" {
#include "stack-c.h"
#include "gw_symbolic.h"
}

#include "scope.hpp"
#include "sym.hpp"

#include "ginac/ginac.h"

using namespace GiNaC;
using namespace std;

IMP_FUNCTION(disp)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);
    
    // Get a sym object
    ex a = Sym::get(1);

    ostringstream s;
    s << a;
    //cout << "Disp: " << s.str() << endl;
    
    String(s.str()).put(1);
    
    return TRUE;
}


IMP_FUNCTION(symbol)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get string matrix
    string a = String::get(1);
    cout << "Symbol: " << a << endl;

    string ref = scope.new_symbol(a);

    Sym(ref).put(1);

    cout << "Guardado" << endl;
    
    return TRUE;
}

IMP_FUNCTION(number)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);
    
    numeric res;
    
    if(String::check(1))
    {
        string a = String::get(1);
        res = numeric(a.c_str());
    }
    else
    {
        double a = Double::get(1);
        res = numeric(a);
    }

    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(Pi)
{
    // Check input and output size
    CheckLhs(1,1);
    Rhs=Max(0,Rhs);
    
    Sym("pi").put(1);

    return TRUE;
}

IMP_FUNCTION(Catalan)
{

    // Check input and output size
    CheckLhs(1,1);
    Rhs=Max(0,Rhs);

    Sym("catalan").put(1);

    return TRUE;
}

IMP_FUNCTION(Euler)
{
    // Check input and output size
    CheckLhs(1,1);
    Rhs=Max(0,Rhs);
    
    Sym("euler").put(1);

    return TRUE;
}

IMP_FUNCTION(add)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    ex a  = Sym::get(1);
    ex b  = Sym::get(2);

    // Do something
    ex res = a+b;

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(sub)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    ex a  = Sym::get(1);
    ex b  = Sym::get(2);

    // Do something
    ex res = a-b;

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(mul)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    ex a  = Sym::get(1);
    ex b  = Sym::get(2);

    // Do something
    ex res = a*b;

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(res)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    ex a  = Sym::get(1);
    ex b  = Sym::get(2);

    // Do something
    ex res = a/b;

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(pow)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    ex a  = Sym::get(1);
    ex b  = Sym::get(2);

    // Do something
    ex res = pow(a,b);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(set_digits)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    unsigned int n = UInteger::get(1);

    // Do something
    Digits = n;

    // Return data
    UInteger(Digits).put(1);
    return TRUE;
}

IMP_FUNCTION(get_digits)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(0,0);

    // Get operands

    // Do something

    // Return data
    UInteger(Digits).put(1);
    return TRUE;
}

// General
IMP_FUNCTION(evalf)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex r = Sym::get(1);

    // Do something
    r = r.evalf();

    // Return data
    Sym(r).put(1);
    return TRUE;
}

IMP_FUNCTION(expand)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex r = Sym::get(1);

    // Do something
    r = r.expand();

    // Return data
    Sym(r).put(1);
    return TRUE;
}

IMP_FUNCTION(collect)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    ex r = Sym::get(1);
    ex s = Sym::get(2);

    // Do something
    // TODO: Add colllect_common_factor
    // TODO: Distributed
    r = r.collect(s);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

IMP_FUNCTION(subs)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(3,3);

    // Get operands
    ex a = Sym::get(1);
    ex b = Sym::get(2);
    ex c = Sym::get(3);

    // Do something
    lst lb = is_a<lst>(b) ? ex_to<lst>(b) : lst(b);
    lst lc = is_a<lst>(c) ? ex_to<lst>(c) : lst(c);
    
    ex r = a.subs(lb, lc);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

IMP_FUNCTION(scope_clean)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(0,0);

    // Get operands

    // Do something
    scope.scope.clear();
    scope.init_constants();

    // Return data
    UInteger(0).put(1);
    return TRUE;
}

IMP_FUNCTION(scope_list)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(0,0);

    // Get operands

    // Do something
    map<string, ex>::iterator it;
    
    ostringstream s;
    
    int i=0;
    for (it =scope.scope.begin(); it != scope.scope.end();it++)
        s << endl << "<" << it->first << ">";
    
    // Return data
    String(s.str()).put(1);
    return TRUE;
}

IMP_FUNCTION(scope_get)
{

    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    string a = String::get(1);
    
    // Do something
    if (scope.scope.count(a) < 1)
        Scierror(999, "Reference invalid.");

    // Return data
    Sym(a).put(1);
    return TRUE;
}

// Relations
IMP_FUNCTION(rel_equal)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    ex a = Sym::get(1);
    ex b = Sym::get(2);
    
    // Do something
    ex c = (a==b);


    // Return data
    Sym(c).put(1);
    return TRUE;
}

IMP_FUNCTION(rel_not_equal)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    ex a = Sym::get(1);
    ex b = Sym::get(2);
    
    // Do something
    ex c = (a!=b);

    // Return data
    Sym(c).put(1);
    return TRUE;
}

IMP_FUNCTION(rel_less)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    ex a = Sym::get(1);
    ex b = Sym::get(2);
    
    // Do something
    ex c = (a<b);

    // Return data
    Sym(c).put(1);
    return TRUE;
}

IMP_FUNCTION(rel_less_or_equal)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    ex a = Sym::get(1);
    ex b = Sym::get(2);
    
    // Do something
    ex c = (a<=b);

    // Return data
    Sym(c).put(1);
    return TRUE;
}

IMP_FUNCTION(rel_greater)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    ex a = Sym::get(1);
    ex b = Sym::get(2);
    
    // Do something
    ex c = (a>b);

    // Return data
    Sym(c).put(1);
    return TRUE;
}

IMP_FUNCTION(rel_greater_or_equal)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    ex a = Sym::get(1);
    ex b = Sym::get(2);
    
    // Do something
    ex c = (a>=b);

    // Return data
    Sym(c).put(1);
    return TRUE;
}


// Checks
IMP_FUNCTION(is_numeric)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a = Sym::get(1);
    
    // Do something
    bool c = a.info(info_flags::numeric);

    // Return data
    Bool(c).put(1);
    return TRUE;
}

IMP_FUNCTION(is_real)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a = Sym::get(1);
    
    // Do something
    bool c = a.info(info_flags::real);

    // Return data
    Bool(c).put(1);
    return TRUE;
}

IMP_FUNCTION(is_rational)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a = Sym::get(1);
    
    // Do something
    bool c = a.info(info_flags::rational);
    
    // Return data
    Bool(c).put(1);
    return TRUE;
}

IMP_FUNCTION(is_integer)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a = Sym::get(1);
    
    // Do something
    bool c = a.info(info_flags::integer);

    // Return data
    Bool(c).put(1);
    return TRUE;
}

IMP_FUNCTION(is_crational)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a = Sym::get(1);
    
    // Do something
    bool c = a.info(info_flags::crational);

    // Return data
    Bool(c).put(1);
    return TRUE;
}

IMP_FUNCTION(is_cinteger)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a = Sym::get(1);
    
    // Do something
    bool c = a.info(info_flags::cinteger);

    // Return data
    Bool(c).put(1);
    return TRUE;
}

IMP_FUNCTION(is_positive)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a = Sym::get(1);
    
    // Do something
    bool c = a.info(info_flags::positive);

    // Return data
    Bool(c).put(1);
    return TRUE;
}

IMP_FUNCTION(is_negative)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a = Sym::get(1);
    
    // Do something
    bool c = a.info(info_flags::negative);

    // Return data
    Bool(c).put(1);
    return TRUE;
}

IMP_FUNCTION(is_nonnegative)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a = Sym::get(1);
    
    // Do something
    bool c = a.info(info_flags::nonnegative);

    // Return data
    Bool(c).put(1);
    return TRUE;
}

IMP_FUNCTION(is_posint)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a = Sym::get(1);
    
    // Do something
    bool c = a.info(info_flags::posint);

    // Return data
    Bool(c).put(1);
    return TRUE;
}

IMP_FUNCTION(is_negint)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a = Sym::get(1);
    
    // Do something
    bool c = a.info(info_flags::negint);

    // Return data
    Bool(c).put(1);
    return TRUE;
}

IMP_FUNCTION(is_nonnegint)

{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a = Sym::get(1);
    
    // Do something
    bool c = a.info(info_flags::nonnegint);

    // Return data
    Bool(c).put(1);
    return TRUE;
}

IMP_FUNCTION(is_even)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a = Sym::get(1);
    
    // Do something
    bool c = a.info(info_flags::even);

    // Return data
    Bool(c).put(1);
    return TRUE;
}

IMP_FUNCTION(is_odd)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a = Sym::get(1);
    
    // Do something
    bool c = a.info(info_flags::odd);

    // Return data
    Bool(c).put(1);
    return TRUE;
}

IMP_FUNCTION(is_prime)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a = Sym::get(1);
    
    // Do something
    bool c = a.info(info_flags::prime);

    // Return data
    Bool(c).put(1);
    return TRUE;
}

IMP_FUNCTION(is_rel)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a = Sym::get(1);
    
    // Do something
    bool c = a.info(info_flags::relation);

    // Return data
    Bool(c).put(1);
    return TRUE;
}

IMP_FUNCTION(is_rel_equal)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a = Sym::get(1);
    
    // Do something
    bool c = a.info(info_flags::relation_equal);

    // Return data
    Bool(c).put(1);
    return TRUE;
}

IMP_FUNCTION(is_rel_not_equal)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a = Sym::get(1);
    
    // Do something
    bool c = a.info(info_flags::relation_not_equal);

    // Return data
    Bool(c).put(1);
    return TRUE;
}

IMP_FUNCTION(is_rel_less)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a = Sym::get(1);
    
    // Do something
    bool c = a.info(info_flags::relation_less);

    // Return data
    Bool(c).put(1);
    return TRUE;
}

IMP_FUNCTION(is_rel_less_or_equal)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a = Sym::get(1);
    
    // Do something
    bool c = a.info(info_flags::relation_less_or_equal);

    // Return data
    Bool(c).put(1);
    return TRUE;
}

IMP_FUNCTION(is_rel_greater)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a = Sym::get(1);
    
    // Do something
    bool c = a.info(info_flags::relation_greater);

    // Return data
    Bool(c).put(1);
    return TRUE;
}

IMP_FUNCTION(is_rel_gerater_or_equal)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a = Sym::get(1);
    
    // Do something
    bool c = a.info(info_flags::relation_greater_or_equal);

    // Return data
    Bool(c).put(1);
    return TRUE;
}



