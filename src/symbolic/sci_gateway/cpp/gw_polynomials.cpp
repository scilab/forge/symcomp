/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009 - Jorge CARDONA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
 

extern "C" {
#include "stack-c.h"
#include "gw_polynomials.h"
}

#include "polynomials.hpp"

#include "scope.hpp"
#include "sym.hpp"

#include "ginac/ginac.h"

using namespace GiNaC;
using namespace std;

// Return the degree of a polynomial
IMP_FUNCTION(poly_degree)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    ex a  = Sym::get(1);
    ex x  = Sym::get(2);

    // Do something
    unsigned int r = a.degree(x);

    // Return data
    UInteger(r).put(1);
    return TRUE;
}

// Return the low degree of a polynomial
IMP_FUNCTION(poly_ldegree)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    ex a  = Sym::get(1);
    ex x  = Sym::get(2);

    // Do something
    unsigned int r = a.ldegree(x);

    // Return data
    UInteger(r).put(1);
    return TRUE;
}

// Return the n-th coeff of a polynomial
IMP_FUNCTION(poly_coeff)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(3,3);

    // Get operands
    ex a            = Sym::get(1);
    ex x            = Sym::get(2);
    unsigned int p  = UInteger::get(3);

    // Do something
    ex r = a.coeff(x,p);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

// Return the leading coeff of a polynomial
IMP_FUNCTION(poly_lcoeff)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    ex a  = Sym::get(1);
    ex x  = Sym::get(2);

    // Do something
    ex r = a.lcoeff(x);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

// Return the trailing coeff of a polynomial
IMP_FUNCTION(poly_tcoeff)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    ex a  = Sym::get(1);
    ex x  = Sym::get(2);

    // Do something
    ex r = a.tcoeff(x);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

// Return the quotient of a plynomial division (a/b) and satisfy a = b*q + r
IMP_FUNCTION(poly_quo)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(3,3);

    // Get operands
    ex a  = Sym::get(1);
    ex b  = Sym::get(2);
    ex x  = Sym::get(3);
    
    // Do something
    ex r = quo(a,b,x);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

// Return the remainder of a plynomial division (a/b) and satisfy a = b*q + r
IMP_FUNCTION(poly_rem)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(3,3);

    // Get operands
    ex a  = Sym::get(1);
    ex b  = Sym::get(2);
    ex x  = Sym::get(3);
    
    // Do something
    ex r = rem(a,b,x);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

// Return the gcd between two polynomials
IMP_FUNCTION(poly_gcd)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    ex a  = Sym::get(1);
    ex b  = Sym::get(2);
    
    // Do something
    ex r = gcd(a,b);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

// Return the lcm between two polynomials
IMP_FUNCTION(poly_lcm)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    ex a  = Sym::get(1);
    ex b  = Sym::get(2);
    
    // Do something
    ex r = lcm(a,b);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

// Return the resultant between polynomials a and b.
IMP_FUNCTION(poly_resultant)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(3,3);

    // Get operands
    ex a  = Sym::get(1);
    ex b  = Sym::get(2);
    ex x  = Sym::get(3);
    
    // Do something
    ex r = resultant(a,b,x);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

// Return the square free factorization of a polynomial
IMP_FUNCTION(poly_sqrfree)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    ex a    = Sym::get(1);
    lst l   = Sym::get(2);
    
    // Do something
    ex r = sqrfree(a, l);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

// Return the factorization of a polynomial
IMP_FUNCTION(poly_factor)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);
    
    // Do something
    ex r = factor(a);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

// Return the chebyshev polynomial of first kind
IMP_FUNCTION(poly_chebyshev_T)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    numeric n   = Sym::get(1);
    symbol x    = Sym::get(2);
    
    // Do something
    ex r = chebyshev_1(n, x);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

// Return the chebyshev polynomial of second kind
IMP_FUNCTION(poly_chebyshev_U)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    numeric n   = Sym::get(1);
    symbol x    = Sym::get(2);
    
    // Do something
    ex r = chebyshev_2(n, x);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

// Return the legendre polynomial
IMP_FUNCTION(poly_legendre)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    numeric n   = Sym::get(1);
    symbol x    = Sym::get(2);
    
    // Do something
    ex r = legendre(n, x);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

// Return the jacobi polynomial
IMP_FUNCTION(poly_jacobi)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(4,4);

    // Get operands
    numeric n   = Sym::get(1);
    ex a        = Sym::get(2);
    ex b        = Sym::get(3);
    symbol x    = Sym::get(4);
    
    // Do something
    ex r = jacobi(n, a, b, x);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

// Return the laguerre polynomial
IMP_FUNCTION(poly_laguerre)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    numeric n   = Sym::get(1);
    symbol x    = Sym::get(2);
    
    // Do something
    ex r = laguerre(n, x);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

// Return the gegenbauer polynomial
IMP_FUNCTION(poly_gegenbauer)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(3,3);

    // Get operands
    numeric n   = Sym::get(1);
    ex a        = Sym::get(2);
    symbol x    = Sym::get(3);
    
    // Do something
    ex r = gegenbauer(n, a, x);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

