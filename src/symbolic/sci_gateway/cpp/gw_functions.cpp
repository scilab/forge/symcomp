/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009 - Jorge CARDONA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
 
extern "C" {
#include "stack-c.h"
#include "gw_functions.h"
}


#include "scope.hpp"
#include "sym.hpp"
#include "ginac/ginac.h"

using namespace GiNaC;
using namespace std;

IMP_FUNCTION(sin)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = sin(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(cos)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = cos(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(tan)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = tan(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(csc)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = 1/sin(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(sec)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = 1/cos(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(cot)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = 1/tan(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}


IMP_FUNCTION(sinh)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = sinh(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(cosh)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = cosh(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(tanh)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = tanh(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(csch)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = 1/sinh(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(sech)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = 1/cosh(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(coth)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = 1/tanh(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}


IMP_FUNCTION(asin)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = asin(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(acos)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = acos(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(atan)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = atan(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(acsc)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = asin(1/a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(asec)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = acos(1/a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(acot)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = atan(1/a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}


IMP_FUNCTION(asinh)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = asinh(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(acosh)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = acosh(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(atanh)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = atanh(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(acsch)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = asinh(1/a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(asech)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = acosh(1/a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(acoth)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = atanh(1/a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}


IMP_FUNCTION(abs)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = abs(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(step)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = step(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(sqrt)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = sqrt(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(exp)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = exp(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(log)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = log(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(gamma)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = tgamma(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(log_gamma)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = lgamma(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(beta)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    ex a  = Sym::get(1);
    ex b  = Sym::get(2);

    // Do something
    ex res = beta(a,b);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(psi)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1);

    // Do something
    ex res = psi(a);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(new_function)
{try{

    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);
    // Get string matrix
    string a        = String::get(1);
    unsigned int b  = UInteger::get(2);
    
    string r = scope.new_function(a.c_str(), b);

    Sym(r).put(1);

    return TRUE;

} catch(logic_error e){
Scierror(999,(char*)(e.what()));
} catch(...){
Scierror(999,"Unexpected error.");
}}

IMP_FUNCTION(call_function)
{
    // Check input and output size
    CheckLhs(1,1);CheckRhs(1,INT_MAX);

    // Get operands
    function a = Sym::get(1);
    exvector arg;
    arg.reserve(Rhs - 1);
    for(int i = 2; i <= Rhs; i++)
        arg.push_back(Sym::get(i));
        
    // Do something
    ex res = function(a.get_serial(), arg);

    // Return data
    Sym(res).put(1);
    return TRUE;
}


