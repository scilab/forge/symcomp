/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009 - Jorge CARDONA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
 
extern "C" {
#include "stack-c.h"
#include "gw_combinatorics.h"
}

#include "scope.hpp"
#include "sym.hpp"
#include "combinatorics.hpp"

#include "ginac/ginac.h"

using namespace GiNaC;
using namespace std;

IMP_FUNCTION(factorial)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex x  = Sym::get(1);

    // Do something
    ex r = factorial(x);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

IMP_FUNCTION(factorial_rising)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    numeric n   = Sym::get(1);
    ex x        = Sym::get(2);

    // Do something
    ex r = factorial_rising(n,x);

    // Return data
    Sym(r).put(1);
    return TRUE;
}


IMP_FUNCTION(factorial_falling)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    numeric n   = Sym::get(1);
    ex x        = Sym::get(2);

    // Do something
    ex r = factorial_falling(n,x);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

