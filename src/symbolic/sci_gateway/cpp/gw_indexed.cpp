/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009 - Jorge CARDONA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
 
extern "C" {
#include "stack-c.h"
#include "gw_indexed.h"
}

#include "scope.hpp"
#include "sym.hpp"
#include "ginac/ginac.h"

using namespace GiNaC;
using namespace std;
//TODO: What about indexed_dimension???

IMP_FUNCTION(index)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,4);

    // Get operands
    ex a  = Sym::get(1);
    ex b;
    if(UInteger::check(2))
        b = numeric(static_cast<unsigned int>(UInteger::get(2)));
    else
        b = Sym::get(2);
        
    bool v, d;
    if (Rhs > 2 )
        v = Bool::get(3);
    if (Rhs > 3 )
        d = Bool::get(4);


    // Do something
    ex res;
    if (Rhs == 2)
        res = idx(a,b);
        
    else if (Rhs == 3)
        res = varidx(a,b,v);
        
    else
        res = spinidx(a,b,v,d);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(indexed)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,INT_MAX);

    // Get operands
    ex a = Sym::get(1);
    ex b = Sym::get(2);

    exvector v;
    indexed res;
    if(is_a<symmetry>(b))
    {
        v.reserve(Rhs - 2);
        for(int i = 3; i <= Rhs; i++)
            v.push_back(Sym::get(i));
    
        // Do something
        res = indexed(a,ex_to<symmetry>(b),v);
    }
    else
    {
        v.reserve(Rhs - 1);
        for(int i = 2; i <= Rhs; i++)
            v.push_back(Sym::get(i));
    
        // Do something
        res = indexed(a, v);
    }
    
    // Return data
    Sym(res).put(1);
    return TRUE;
}

// Return the value of the index
IMP_FUNCTION(index_value)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    idx a  = Sym::get(1);

    // Do something
    ex res = a.get_value();

    // Return data
    Sym(res).put(1);
    return TRUE;
}

// Return the dimension of the index
IMP_FUNCTION(index_dim)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    idx a  = Sym::get(1);

    // Do something
    ex res = a.get_dim();

    // Return data
    Sym(res).put(1);
    return TRUE;
}

// Return true if it has a numeric value
IMP_FUNCTION(index_is_numeric)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    idx a  = Sym::get(1);

    // Do something
    bool res = a.is_numeric();

    // Return data
    Bool(res).put(1);
    return TRUE;
}

// Return true if it has a symbolic value
IMP_FUNCTION(index_is_symbolic)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    idx a  = Sym::get(1);

    // Do something
    bool res = a.is_symbolic();

    // Return data
    Bool(res).put(1);
    return TRUE;
}

// Return true if it has a numeric dim
IMP_FUNCTION(index_is_dim_numeric)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    idx a  = Sym::get(1);

    // Do something
    bool res = a.is_dim_numeric();

    // Return data
    Bool(res).put(1);
    return TRUE;
}

// Return true if it has a symbolc dim
IMP_FUNCTION(index_is_dim_symbolic)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    idx a  = Sym::get(1);

    // Do something
    bool res = a.is_dim_symbolic();

    // Return data
    Bool(res).put(1);
    return TRUE;
}

// Return true if it is contravariant
IMP_FUNCTION(index_is_contravariant)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1)();

    // Do something
    bool res;
    if(is_a<varidx>(a))
        res = ex_to<varidx>(a).is_contravariant();
    else
        Scierror(999,"Index should has variance.");

        
    // Return data
    Bool(res).put(1);
    return TRUE;
}

// Return true if it is covariant
IMP_FUNCTION(index_is_covariant)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1)();

    // Do something
    bool res;
    if(is_a<varidx>(a))
        res = ex_to<varidx>(a).is_covariant();
    else
        Scierror(999,"Index should has variance.");
        
    // Return data
    Bool(res).put(1);
    return TRUE;
}

// Return true if it is dotted
IMP_FUNCTION(index_is_dotted)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1)();

    // Do something
    bool res;
    if(is_a<spinidx>(a))
        res = ex_to<spinidx>(a).is_dotted();
    else
        Scierror(999,"Index should has dotness.");
        
    // Return data
    Bool(res).put(1);
    return TRUE;
}

// Return true if it is undotted
IMP_FUNCTION(index_is_undotted)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1)();

    // Do something
    bool res;
    if(is_a<spinidx>(a))
        res = ex_to<spinidx>(a).is_undotted();
    else
        Scierror(999,"Index should has dotness.");
        
    // Return data
    Bool(res).put(1);
    return TRUE;
}
// Return a new index with the inverse variance.
IMP_FUNCTION(index_toggle_variance)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1)();

    // Do something
    ex res;
    if (is_a<varidx>(a))
        res = ex_to<varidx>(a).toggle_variance();
    else if(is_a<spinidx>(a))
        res = ex_to<spinidx>(a).toggle_variance_dot();
    else
        Scierror(999,"Index should has variance.");
        
    // Return data
    Sym(res).put(1);
    return TRUE;
}

// Return a new index with the inverse dottness???.
IMP_FUNCTION(index_toggle_dot)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex a  = Sym::get(1)();

    // Do something
    ex res;
    if(is_a<spinidx>(a))
        res = ex_to<spinidx>(a).toggle_dot();
    else
        Scierror(999,"Index should has dotness.");

    // Return data
    Sym(res).put(1);
    return TRUE;
}

// Return none symmetry
IMP_FUNCTION(sy_none)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(0,INT_MAX);

    // Get operands
    symmetry s = sy_none();
    
    for(int i = 1 ; i <= Rhs; i++)
    {
        if(UInteger::check(i))
            s.add(static_cast<unsigned int>(UInteger::get(i)));
        else
        {
            ex a  = Sym::get(i);
            if (is_a<symmetry>(a))
                s.add(ex_to<symmetry>(a));
            else
                Scierror(999,"Argument %d should be an uint32 or a symmetry.",i);
        }
    }

    // Return data
    Sym(s).put(1);
    return TRUE;
}
// Return full symmetry
IMP_FUNCTION(sy_symm)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(0,INT_MAX);

    // Get operands
    symmetry s = sy_symm();
    
    for(int i = 1 ; i <= Rhs; i++)
    {
        if(UInteger::check(i))
            s.add(static_cast<unsigned int>(UInteger::get(i)));
        else
        {
            ex a  = Sym::get(i);
            if (is_a<symmetry>(a))
                s.add(ex_to<symmetry>(a));
            else
                Scierror(999,"Argument %d should be an uint32 or a symmetry.",i);
        }
    }

    // Return data
    Sym(s).put(1);
    return TRUE;
}

// Return anti symmetry
IMP_FUNCTION(sy_anti)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(0,INT_MAX);

    // Get operands
    symmetry s = sy_anti();
    
    for(int i = 1 ; i <= Rhs; i++)
    {
        if(UInteger::check(i))
            s.add(static_cast<unsigned int>(UInteger::get(i)));
        else
        {
            ex a  = Sym::get(i);
            if (is_a<symmetry>(a))
                s.add(ex_to<symmetry>(a));
            else
                Scierror(999,"Argument %d should be an uint32 or a symmetry.",i);
        }
    }

    // Return data
    Sym(s).put(1);
    return TRUE;
}

// Return cyclic symmetry
IMP_FUNCTION(sy_cycl)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(0,INT_MAX);

    // Get operands
    symmetry s = sy_cycl();
    
    for(int i = 1 ; i <= Rhs; i++)
    {
        if(UInteger::check(i))
            s.add(static_cast<unsigned int>(UInteger::get(i)));
        else
        {
            ex a  = Sym::get(i);
            if (is_a<symmetry>(a))
                s.add(ex_to<symmetry>(a));
            else
                Scierror(999,"Argument %d should be an uint32 or a symmetry.",i);
        }
    }

    // Return data
    Sym(s).put(1);
    return TRUE;
}

// Return the free index on a expression
IMP_FUNCTION(get_free_indices)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex e = Sym::get(1);
    
    // Do something
    ex res = exprseq(e.get_free_indices());
    
    // Return data
    Sym(res).put(1);
    return TRUE;
}

// Expand sum terms with dummy indices
IMP_FUNCTION(expand_dummy_sum)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex e = Sym::get(1);

    // Do something
    ex res = expand_dummy_sum(e);
    
    // Return data
    Sym(res).put(1);
    return TRUE;
}

