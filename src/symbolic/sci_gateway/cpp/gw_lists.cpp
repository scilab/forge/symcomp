/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009 - Jorge CARDONA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
 
extern "C" {
#include "stack-c.h"
#include "gw_lists.h"
}

#include "scope.hpp"
#include "sym.hpp"

#include "ginac/ginac.h"

using namespace GiNaC;
using namespace std;

IMP_FUNCTION(list)
{
    // Check input and output size
    CheckLhs(1,1);CheckRhs(0,INT_MAX);

    exvector v;
    v.reserve(Rhs);
    for(int i = 1; i <= Rhs; i++)
        v.push_back(Sym::get(i));

    // Do something
    lst res(v.begin(), v.end());

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(list_append)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    lst a   = Sym::get(1);
    ex b    = Sym::get(2);

    // Do something
    a.append(b);

    // Return data
    Sym(a).put(1);
    return TRUE;
}

IMP_FUNCTION(list_prepend)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    lst a   = Sym::get(1);
    ex b    = Sym::get(2);

    // Do something
    a.prepend(b);

    // Return data
    Sym(a).put(1);
    return TRUE;
}

IMP_FUNCTION(list_item_get)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    lst a           = Sym::get(1);
    unsigned int b  = UInteger::get(2);

    // Do something
    ex res = a.op(b-1);

    // Return data
    Sym(res).put(1);
    return TRUE;
}

IMP_FUNCTION(list_item_set)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(3,3);

    // Get operands
    lst a           = Sym::get(1);
    unsigned int b  = UInteger::get(2);
    ex c            = Sym::get(3);

    // Do something
    a.op(b-1) = c;

    // Return data
    Sym(a).put(1);
    return TRUE;
}

IMP_FUNCTION(list_remove_first)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    lst a  = Sym::get(1);

    // Do something
    a.remove_first();

    // Return data
    Sym(a).put(1);
    return TRUE;
}

IMP_FUNCTION(list_remove_last)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    lst a  = Sym::get(1);

    // Do something
    a.remove_last();

    // Return data
    Sym(a).put(1);
    return TRUE;
}

IMP_FUNCTION(list_clear)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    lst a  = Sym::get(1);

    // Do something
    a.remove_all();

    // Return data
    Sym(a).put(1);
    return TRUE;
}

IMP_FUNCTION(list_size)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    lst a  = Sym::get(1);

    // Do something
    unsigned int s = a.nops();

    // Return data
    UInteger(s).put(1);
    return TRUE;
}

IMP_FUNCTION(list_concatenation)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    lst a  = Sym::get(1);
    lst b  = Sym::get(2);
    
    // Do something
    lst r;
    int n_a = a.nops();
    int n_b = b.nops();
    
    // With iterators we get O(n) with op(i) is O(n^2)
    for(lst::const_iterator i = a.begin(); i != a.end(); ++i)
        r.append(*i);
        
    for(lst::const_iterator i = b.begin(); i != b.end(); ++i)
        r.append(*i);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

